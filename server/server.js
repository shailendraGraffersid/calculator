require('dotenv').config();

const express = require('express');
let bodyParser = require('body-parser')
const mongoose = require('mongoose');
let cors = require('cors');
const routes = require('./routes/routes');
const mongoString = process.env.DATABASE_URL;

const app = express();

mongoose.connect(mongoString);
const database = mongoose.connection;

database.on('error', (error) => {
    console.log(error)
})

database.once('connected', () => {
    console.log('Database Connected');
})

app.use(cors());
app.use(express.static('uploads'));
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: false
}));

app.use('/api', routes)

app.use(express.json());

app.listen(3000, () => {
    console.log(`Server Started at ${3000}`)
})