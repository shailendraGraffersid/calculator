const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema({
    key_data: {
        required: true,
        type: String
    },
    result: {
        required: true,
        type: String
    }
})

module.exports = mongoose.model('Data', dataSchema)