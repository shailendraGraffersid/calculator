const express = require('express');
const Model = require('../models/model');

const router = express.Router()


router.post('/submitResult', async (req, res) => {
    console.log('call api', req.body)
    const data = new Model({
        key_data: req.body.key_data,
        result: req.body.result
    })
    try {
        const dataToSave = await data.save();
        res.status(200).json(dataToSave)
    }
    catch (error) {
        res.status(400).json({message: error.message})
    }
});

router.get('/getAll', async (req, res) => {
    try{
        const data = await Model.find();
        res.json(data)
    }
    catch(error){
        res.status(500).json({message: error.message})
    }
})


module.exports = router;