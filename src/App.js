import { useState } from 'react';
import './App.css';
import axios from 'axios';

import KeyPadComponent from './KeyPadComponent';
import ResultComponent from './ResultComponent';
import Recent from './Recent';

function App() {

  const [result, setResult] = useState("");
  const[recent, setRecent] = useState("");
  const[status, setStatus] = useState(false);

  const onClick = button => {
    if(button === "="){
      calculate()
    }
    else if(button === "C"){
      reset()
    }else if(button === "R"){
      setStatus(!status)
    }
    else {
      setResult(result + button)
      setRecent(result + button)
    }
  };

  const calculate = () => {
    try {
      setResult((eval(result) || "" ) + 0)
      var calc = ((eval(result) || "" ) + 0);
      axios.post("http://localhost:3000/api/submitResult",{
        key_data:recent,
        result:calc
      }).then(res=>{
        console.log("Response", res);
      })
    } catch (e) {
      setResult("error")
    }
  };

  const reset = () => {
    setResult("")
  };

  return (
    <div className="App">
       <div className="calculator-body">
          <h1>Calculator</h1>
          <ResultComponent result={result}/>
         {status? <Recent/>:""}
          <KeyPadComponent onClick={onClick}/>
        </div>

    </div>
  );
}

export default App;
