import React, { useEffect, useState } from "react";
import axios from "axios";

export default function Recent(){
const [list, setlist] = useState([]);

useEffect(()=>{
    axios.get("http://localhost:3000/api/getAll").then(res=>{
        console.log("Recent list", res.data);
        setlist(res.data);
    })
},[])

    return(
        <>
        {list.map((item, index)=>{
            return(
                <>
                <div>
            {item.key_data}={item.result}
        </div>
                </>
            )
        })}
        
        </>
    )
}